package jca

import org.apache.spark.sql.{DataFrame, SparkSession}

object EjerciciosSparkSQL extends App {
  val spark = SparkSession.builder()
    .appName("OpcionalSparkSQL")
    .master("local[1]")
    .getOrCreate()
  // Reading a file
  val simpsons = spark.read.option("header", value = true).csv("src/main/resources/simpsons.csv")
  simpsons.show(5)

  // Creating a temporal view/table from a DataFrame
  simpsons.createOrReplaceTempView("simpsons")

  //querying season average rating
  val seasonRating: DataFrame = spark.sql("select cast(season as int), avg(imdb_rating) from simpsons group by season order by cast(season as int) ")

  seasonRating.show()
}
