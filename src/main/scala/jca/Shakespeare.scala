package jca

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.functions.col

object Shakespeare extends App with spark {
  /*val zips: DataFrame = readFromFileJSON("C:\\Users\\jaime.casado\\Desktop\\CarpetaCompartidaFundBigData\\data_spark\\zips.json")
   zips.show(5)
   zips.filter(zips("pop") > 10000).collect().foreach(println)

    zips.createOrReplaceTempView("zips")

    spark.sql("select * from zips where pop < 10000").take(5).foreach(println)

    spark.sql("select city from zips group by city having count(*)>100").take(5).foreach(println)

    spark.sql("select sum(pop) from zips where state = 'WI'").take(5).foreach(println)

    spark.sql("select state from zips group by state order by sum(pop) desc limit 5").collect().foreach(println)

    val datosPartidos = readFromFileCSVWithDelimiter("C:\\Users\\jaime.casado\\Desktop\\CarpetaCompartidaFundBigData\\data_spark\\DataSetPartidos.txt", "::")
    val stringScheme = "idPartido::temporada::jornada::EquipoLocal::EquipoVisitante::golesLocal::golesVisitante::fecha::timestamp"
    val scheme = StructType(stringScheme.split("::").map(StructField(_,dataType = StringType, nullable = true)))
    val datosPartidosConEsquema = spark.createDataFrame(datosPartidos.rdd, scheme)

    datosPartidosConEsquema.createOrReplaceTempView("partidos")
    val results = spark.sql("select temporada, jornada from partidos")
    results.show()
    val recordOviedo = spark.sql("select sum(golesVisitante) from partidos where equipoVisitante like '%Oviedo%' group by temporada").take(1)

    val temporadasOviedo = spark.sql("select count(distinct(temporada)) from partidos where equipoVisitante like '%Oviedo%' or equipoLocal like '%Oviedo%'")
    val temporadasSporting = spark.sql("select count(distinct(temporada)) from partidos where equipoVisitante like '%Sporting%' or equipoLocal like '%Sporting%'")
    println("Oviedo : " + temporadasOviedo + "\nGijon : " + temporadasSporting)

    val simpsons = spark.read.option("header", value = true).csv("C:\\Users\\jaime.casado\\Desktop\\CarpetaCompartidaFundBigData\\data_spark\\simpsons.csv")
    simpsons.show(5)*/

  import spark.implicits._

  val books: Dataset[String] = spark.read.textFile("C:/Users/jaime.casado/Desktop/CarpetaCompartidaFundBigData/shakespeare/*")
  //books.show(5)

  val stopWords: Dataset[String] = spark.read.textFile("C:/Users/jaime.casado/Desktop/CarpetaCompartidaFundBigData/stop-word-list.csv").flatMap(_.split(",")).map(_.replace(" ", ""))
 // stopWords.distinct().where("value like 'the%'").show()

  val keyValueDS = books.map(_.replaceAll("[^a-zA-Z]+", " ")).flatMap(linea => linea.split(" ")).map(_.toLowerCase())
  //keyValueDS.show(5)
  val subtracted = keyValueDS.join(stopWords, keyValueDS("value") === stopWords("value"),"anti").as[String]
  //subtracted.where(col("value").like("the%")).show(5)
  val mapDS = subtracted.map(word => (word, 1))
  val groupedByKey = mapDS.groupByKey(_._1)
  val mappedValues = groupedByKey.mapValues(_._2)
  val reducedByKey = mappedValues.reduceGroups(_ + _)
  //reducedByKey.show(50)
  val sorted = reducedByKey.map(_.swap).orderBy(col("_1").desc).map(_.swap).filter(_._1.length > 1)
  sorted.show(50)


}
