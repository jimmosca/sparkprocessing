package jca

object SparkSessionTest extends App with spark {

  spark.sparkContext.setLogLevel("ERROR")
  println("First SparkContext:")
  println("APP Name :" + spark.sparkContext.appName)
  println("Deploy Mode :" + spark.sparkContext.deployMode)
  println("Master :" + spark.sparkContext.master)

  val data = Seq(
    (10, "Spark"),
    (11, "POM"),
    (12, "Scala")
  )

  val rdd = spark.sparkContext.parallelize(data)
  rdd.foreach(item => {
    println(item)
    println("Estoy en foreach")
  })

  rdd.saveAsTextFile("C:\\Users\\jaime.casado\\Desktop\\CarpetaCompartidaFundBigData\\prueba_write")


  // val irisDF: DataFrame = readFromFile("C:\\Users\\jaime.casado\\Desktop\\CarpetaCompartidaFundBigData\\iris_completo.txt").limit(10)
  //writeToFile(irisDF,"C:\\Users\\jaime.casado\\Desktop\\CarpetaCompartidaFundBigData\\iris_completo_1.txt")
}
