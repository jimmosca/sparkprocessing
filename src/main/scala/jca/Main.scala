import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions.col
object Main extends App {
  println("hello world")
  implicit val spark = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL basic example")
    .config("spark.some.config.option", "some-value")
    .getOrCreate()
  import spark.implicits._
  spark.sparkContext.setLogLevel("ERROR")

  pruebaDavid
  pruebaExceptAll
  val books: Dataset[String] = spark.read.textFile("src/main/resources/shakespeare/*")
  println("books")
  books.show(5)
  println("----")

  val stopWords: Dataset[String] = spark.read.textFile("src/main/resources/stop-word-list.csv").flatMap(_.split(",")).map(_.replace(" ", ""))
  println("stopWords")
  stopWords.show(5)
  println("----")

  println("stopWords where")
  stopWords.distinct().where("value like 'the%'").show()
  println("----")

  val keyValueDS = books.map(_.replaceAll("[^a-zA-Z]+", " ")).flatMap(linea => linea.split(" ")).map(_.toLowerCase())
  println("keyValueDS")
  keyValueDS.na.drop().show(5)
  println("----")

  val subtracted: Dataset[String] = keyValueDS.except(stopWords)
  println("subtracted")
  subtracted.distinct().where("value like 'the%'").show(50)
  println("----")

  val mapDS = subtracted.map(word => (word, 1))
  val groupedByKey = mapDS.groupByKey(_._1)
  val mappedValues = groupedByKey.mapValues(_._2)
  val reducedByKey = mappedValues.reduceGroups(_ + _)
  //reducedByKey.show(50)
  val sorted = reducedByKey.map(_.swap).orderBy(col("_1").desc).map(_.swap).filter(_._1.length > 1)
  println("sorted")
  sorted.show(5)
  println("----")

  def pruebaDavid = {
    val df1 = List("david","david","noelia","noelia","emma").toDF("names")
    df1.show()
    val df2 = List("david","emma").toDF()
    df2.show()

    val df3 = df1.except(df2)
    df3.show
  }
  def pruebaExceptAll = {
    val df1 = List("david","david","noelia","noelia","emma").toDF("names")
    df1.show()
    val df2 = List("david","emma").toDF()
    df2.show()

    val df3 = df1.exceptAll(df2)
    df3.show
  }
}